const express = require(`express`)
const mongoose = require(`mongoose`)
const dotenv = require(`dotenv`)
const cors = require(`cors`)
const userRoutes = require(`./routes/userRoutes.js`)
const productRoutes = require(`./routes/productRoutes.js`)
const orderRoutes = require(`./routes/orderRoutes.js`)
const cartRoutes = require(`./routes/cartRoutes.js`)
const auth = require(`./auth`)

dotenv.config()

const app = express()
const port = 8080;

// MongoDB Connection
mongoose.connect(`mongodb+srv://cinnamontoast:${process.env.MONGODB_PASSWORD}@cluster0.4pgcafz.mongodb.net/e-commerce?retryWrites=true&w=majority`,{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection
db.on('open',() => console.log(`Connected to MongoDB!`))
// MongoDB Connection END

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended:true}))

app.use('/users', userRoutes)
app.use('/products', productRoutes)
app.use('/orders', orderRoutes)
app.use('/cart', cartRoutes)

app.listen(process.env.PORT || port, () => console.log(`Server is running at localHost:${port}`))