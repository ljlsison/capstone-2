const mongoose = require(`mongoose`)

const cart_schema = new mongoose.Schema({
    userID: {
        type: String,
        required: [true, `UserID is required`]
    },
    orders: [{
        orderID: {
            type: String,
            required: [true, `orderID is required`]
        },
        price: {
            type: Number,
            required: [true, `price is required`]
        },
        quantity: {
            type: Number,
            required: [true, `price is required`]
        },
        subtotal: {
            type: Number,
            default: 0,
        }
    }],
    total: {
        type: Number,
        default: 0
    }
})

module.exports = mongoose.model(`Cart`,cart_schema)