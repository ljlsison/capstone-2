const mongoose = require(`mongoose`)

const order_schema = new mongoose.Schema({
    userID: {
        type: String,
        required: [true, `UserID is required`]
    },
    productID: {
            type: String,
            required: [true, `Product ID is required`]
                },
    quantity: {
            type: Number,
            required: [true, `Quantity is required`]
    },
    totalAmount: {
        type: Number,
        required: [true, `Quantity is required`]
    },
    purchasedOn: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model(`Order`,order_schema)