const Cart = require(`../models/Cart.js`)
const User = require(`../models/User.js`)
const Order = require(`../models/Order.js`)
const Product = require(`../models/Product.js`)
const bcrypt = require('bcrypt')
const auth = require(`../auth.js`)

module.exports.createCart = (data) => {
    if(data.isAdmin){
        return Promise.resolve({
            message: `This feature is for non ADMIN only`
        })
    }

    let new_cart = new Cart({
        userID: data.userID
    })

    return new_cart.save()
    .then((created_cart) => {return {message: `Cart created successfully`}})
    .catch((error) => {return {message: `Something went wrong`}})
}

module.exports.addToCart = async (data) => {
    if(data.isAdmin){
        return Promise.resolve({
            message: `This feature is for non ADMIN only`
        })
    }
    let myOrder = await Order.findById(data.order.orderID)
    let product_id = myOrder.productID
    let productInfo = await Product.findById(product_id)

    let is_cart_updated = await Cart.findById(data.order.cartID)
    .then((cart) => {
        cart.orders.unshift({
            orderID: myOrder._id,
            price: productInfo.price,
            subtotal:productInfo.price*myOrder.quantity,
            quantity: myOrder.quantity,
        })
        return cart.save().then((added_order,error) => {
            if (error){
                return false
            }
            return true
        })  
    })
    .catch((error) => {return {message: `Something went wrong`}})

    let myCart = await Cart.findById(data.order.cartID)

    let old_total = myCart.total

    if(is_cart_updated){
        return Cart.findByIdAndUpdate((data.order.cartID),{
            total: old_total + myCart.orders[0].subtotal
        }).then((updated_total,error) => {
            if(error){
                return {message: `Something went wrong`}
            }

            return {message: `Cart updated successfully`}
        })
        // return Promise.resolve({message: `Cart updated successfully`})
    }

    return false
}