const User = require(`../models/User.js`)
const Product = require(`../models/Product.js`)
const Order = require(`../models/Order.js`)
const bcrypt = require(`bcrypt`)
const auth = require(`../auth.js`)
const { request } = require("express")


// Register new user (added verification for duplicate email)
module.exports.checkIfEmailExists = (data) => {
	return User.find({email: data.email}).then((result) => {
		if(result.length > 0){
			return true
		}

		return false
	})
}

module.exports.register = (data) => {
	let encrypted_password = bcrypt.hashSync(data.password, 10)

	let new_user = new User({
		firstName: data.firstName,
		lastName: data.lastName,
		email: data.email,
		mobileNo: data.mobileNo,
		password: encrypted_password
	})

	return new_user.save().then((created_user, error) => {
		if(error){
			return false 
		}

		return {
			message: 'User successfully registered!'
		}
	})
}

// User authentication
module.exports.login = (data) => {
    return User.findOne({email: data.email}).then((result) => {
        if(result == null){
            return {
                message: `User does not exist!`
            }
        }

        const is_password_correct = bcrypt.compareSync(data.password, result.password)

        if (is_password_correct){
            return {
                accessToken: auth.CreateAccessToken(result)
            }
        }

        return {
            message: `Password is incorrect`
        }
    })
}


// Get single user based on ID from token
module.exports.getProfile = (data) => {

	return User.findById(data.userId,{password:0}).then(result => {

		// Makes the password not be included in the result
		// result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});
};


// Admin a User (added Verification if User is already admin)
module.exports.adminUser = (data) => {
    if (data.isAdmin){
        return User.findById(data.user.userID).then((result) =>{
            if(result.isAdmin){
                return {
                    message: `User is already ADMIN`
                }
            }

            return User.findByIdAndUpdate(data.user.userID, {
                isAdmin: true
            }).then((adminUser,error) => {
                if(error){
                    return{
                    message: `Something went wrong`
                    }    
                }
                return {
                    message: `User has been successfully updated to ADMIN`
                }
            })
        }).catch((error) => {
            return {
                message: `User does not exist`
            }
        })   
    }

    return Promise.resolve({
        message: `You must be an ADMIN to access this`
    })
}