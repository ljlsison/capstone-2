const Product = require(`../models/Product.js`)


// Add product ADMIN only
module.exports.addProduct = (data) => {
    if(data.isAdmin){
        let new_product = new Product({
            name: data.product.name,
            description: data.product.description,
            price: data.product.price
        })
    
        return new_product.save().then((new_product,error) => {
            if(error){
                false
            }
            
            return {
                message: `Product has been successfully added!`
            }
        })
    }
    
    return Promise.resolve({
        message: `You must be ADMIN to add a product.`
    })  
}

// Get all products(ADMIN ONLY)
module.exports.getAllProducts = () => {
return Product.find().then((result) => {
        return result
    })
}

// Get all active products
module.exports.getProducts = () => {
return Product.find({isActive:true}).then((result) => {
        return result
    })
}

// Get single product
module.exports.getSingleProduct = (productID) => {
    return Product.findById(productID).then((result) => {return result})
}

// Update product (Admin Only)
module.exports.updateProduct = (productID, data) => {
    if(data.isAdmin){
        return Product.findByIdAndUpdate(productID, {
            name: data.product.name,
            description: data.product.description,
            price: data.product.price,
            isActive: data.product.isActive
        }).then((updated_product,error) => {
            console.log(updated_product)
            if (error){
                return {
                    messsage: `Something went wrong`
                }
            }
    
            return {
                message: `Product has been successfully updated`
            }
        })
    }
        return Promise.resolve({
        message: `You must be ADMIN to add a product.`
    }) 
}


// Archive product (Admin only)
module.exports.archiveProduct = (productID, data) => {
    if(data.isAdmin){
        return Product.findByIdAndUpdate(productID, {
            isActive: false
        }).then((archived_product,error) => {
            if (error){
                return {
                    messsage: `Something went wrong`
                }
            }
    
            return {
                message: `Product has been successfully updated`
            }
        })
    }
        return Promise.resolve({
        message: `You must be ADMIN to add a product.`
    }) 
}

// unrchive product (Admin only)
module.exports.unarchiveProduct = (productID, data) => {
    if(data.isAdmin){
        return Product.findByIdAndUpdate(productID, {
            isActive: true
        }).then((archived_product,error) => {
            if (error){
                return {
                    messsage: `Something went wrong`
                }
            }
    
            return {
                message: `Product has been successfully updated`
            }
        })
    }
        return Promise.resolve({
        message: `You must be ADMIN to add a product.`
    }) 
}