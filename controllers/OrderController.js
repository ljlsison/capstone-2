const Order = require(`../models/Order`)
const Product = require("../models/Product")
const User = require("../models/User")

// Create order checkout non-admin users only
// Created User and Product verification
module.exports.checkout = (data) => {
    if(data.isAdmin){
        return Promise.resolve({
            message: `This feature is for non ADMIN only`
        })
    }
    return Product.findById(data.order.productID).then((result) => {
        if(result.isActive){
            let new_order = new Order({
                userID: data.userID,
                productID: data.order.productID,
                quantity: data.order.quantity,
                totalAmount: result.price*data.order.quantity,
                purchasedOn: data.order.purchasedOn
            })
            
            return new_order.save().then((new_order,error) => {
                if(error){
                    return false
                }
                return {
                    message: `Order has been successfully made!`
                }
            })
        }
        return Promise.resolve({
            message: `Product is currently unavailable.`
        })  
    }).catch((error) => {return {message: `Product does not exist!`}})
} 

/* // Add to cart, user only
module.exports.addToCart = (data) => {
    if(data.isAdmin){
        return Promise.resolve({
            message: `This feature is for non ADMIN only!`
        })
    }
    return Order.find({userID:data.userID}).then((order) => 
    {
        order.products.push({
            productID: data.order.productID,
            quantity: data.order.quantity
        })

        return order.save().then((addedOrder) => {
            return {
                message: `Order has been successfully added`
            }
        }).catch((error) => {return {message: `Something went wrong`}})
    } 
        
    ).catch((error) => {return {message: `Order does not exist`}})
} */

// Get all orders(Admin Only)
module.exports.allOrders = (data) => {
    if(data.isAdmin){
        return Order.find({}).then((result) => {
            return result
        })
    }

    return Promise.resolve({
        message: `You must be an ADMIN to access this`
    })
}

module.exports.myOrders = (data) => {
    if(data.isAdmin === false){
        return Order.find({userID:data.userID}).then((result) => {
            return result
        })
    }

    return Promise.resolve({
        message: `You do not have access to this`
    })
}

// Get Specific User order
module.exports.getUserOrder = (userID, data) => {
    if(data.isAdmin || userID === data.userID){
        return Order.find({userID: userID}).then((result) => {
            return result
        })
        .catch((error) => {return {message:`User does not exist!`}})
    }
    return Promise.resolve({
        message: `You must be an ADMIN to access this`
    })
    
}

