const express = require('express')
const router = express.Router()
const OrderController = require(`../controllers/OrderController.js`)
const auth = require(`../auth.js`)

// Non-admin user checkout
router.post(`/create-order`, auth.verify, (request, response) => {
    const data = {
        order: request.body,
        userID: auth.decode(request.headers.authorization).id,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    OrderController.checkout(data).then((result) => {
        response.send(result)
    })
})


router.post(`/add-to-cart`, auth.verify, (request, response) => {
const data = {
        order: request.body,
        userID: auth.decode(request.headers.authorization).id,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    OrderController.addToCart(data).then((result) => {
        response.send(result)
    })
})

// Get all orders (admin only)
router.get(`/all-orders`, auth.verify, (request, response) => {
    const data = {
        isAdmin: auth.decode(request.headers.authorization).isAdmin 
    }
    OrderController.allOrders(data).then((result) => {
        response.send(result)
    })
})

// Get my orders
router.get(`/my-orders`, auth.verify, (request, response) => {
    const data = {
        userID: auth.decode(request.headers.authorization).id,
        isAdmin: auth.decode(request.headers.authorization).isAdmin 
    }
    OrderController.myOrders(data).then((result) => {
        response.send(result)
    })
})

// Get orders for specific users (admin only)
router.get(`/:userID/get-user-order`, auth.verify, (request, response) => {
    const data = {
        userID: auth.decode(request.headers.authorization).id,
        isAdmin: auth.decode(request.headers.authorization).isAdmin 
    }
    OrderController.getUserOrder(request.params.userID, data).then((result) => {
        response.send(result)
    })
})
module.exports = router