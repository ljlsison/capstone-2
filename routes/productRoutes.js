const express = require('express')
const router = express.Router()
const ProductController = require(`../controllers/ProductController.js`)
const auth = require(`../auth.js`)

// Add product (admin only)
router.post("/add-product", auth.verify, (request, response) => {
    const data = {
        product: request.body,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    ProductController.addProduct(data).then((result) => {
        response.send(result)
    })
})

// Get all active products
router.get("/",(request, response) => {
    ProductController.getProducts().then((result) => {
        response.send(result)
    })
})

// Get all products
router.get("/all", (request, response) => {
    ProductController.getAllProducts().then((result) => {
        response.send(result)
    })
})


// Get single product
router.get('/:productID', (request, response) => {
    ProductController.getSingleProduct(request.params.productID).then((result) => {
        response.send(result)
    })
})

// Update product, admin only
router.patch(`/:productID/update`, auth.verify, (request, response) => {
    const data = {
        product: request.body,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    ProductController.updateProduct(request.params.productID, data).then((result) => {
        response.send(result)
    })
})

// Archive product, admin only
router.patch(`/:productID/archive`, auth.verify, (request, response) => {
    const data = {
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    ProductController.archiveProduct(request.params.productID, data).then((result) => {
        response.send(result)
    })
})

router.patch(`/:productID/unarchive`, auth.verify, (request, response) => {
    const data = {
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    ProductController.unarchiveProduct(request.params.productID, data).then((result) => {
        response.send(result)
    })
})

module.exports = router