const express = require('express')
const router = express.Router()
const UserController = require(`../controllers/UserController.js`)
const auth = require(`../auth.js`)

// Check if email exists
router.post("/check-email", (request, response) => {
	UserController.checkIfEmailExists(request.body).then((result) => {
		response.send(result)
	})
})

// Route for registering User
router.post("/register", (request, response) => {
	UserController.register(request.body).then((result) => {
		response.send(result)
	})
})

// Route for authenticating user
router.post(`/login`,(request, response) => {
    UserController.login(request.body).then((result) => {
        response.send(result)
    }) 
})

// Get user details from token
router.get("/details", auth.verify, (request, response) => {

	// Retrieves the user data from the token
	const user_data = auth.decode(request.headers.authorization);

	// Provides the user's ID for the getProfile controller method
	UserController.getProfile({userId : user_data.id}).then(result => response.send(result));
});


// Update User to Admin route (admin only)
router.patch(`/adminUser`, auth.verify,(request, response) => {
    let data = {
        user: request.body,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    UserController.adminUser(data).then((result) => {
        response.send(result)
    })
})

module.exports = router