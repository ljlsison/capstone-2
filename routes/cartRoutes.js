const express = require('express')
const router = express.Router()
const CartController = require(`../controllers/CartController.js`)
const auth = require(`../auth.js`)

router.post(`/create-cart`,auth.verify,(request, response) => {
    const data = {
        userID: auth.decode(request.headers.authorization).id,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    CartController.createCart(data).then((result) => {
        response.send(result)
    })
})

router.post(`/add-to-cart`, auth.verify, (request, response) => {
const data = {
        order: request.body,
        userID: auth.decode(request.headers.authorization).id,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    CartController.addToCart(data).then((result) => {
        response.send(result)
    })
})

module.exports = router